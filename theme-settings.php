<?php
/**
 * Theme settings for Scaccarium
 */

function scaccarium_settings($saved_settings) {
  $defaults = array(
    'scaccarium_layout' => 'fluid',
  );

  $settings = array_merge($defaults, $saved_settings);

  $form['scaccarium_layout'] = array(
    '#type' => 'radios',
    '#title' => t('Layout'),
    '#description' => t('Fluid - Page will use a full-width layout that scales to any screen size.') .'<br />'. t('Fixed-Width - Page will be set at 960px wide for all screen sizes.'),
    '#default_value' => $settings['scaccarium_layout'],
    '#options' => array(
      'fluid' => t('Fluid'),
      'fixed' => t('Fixed-Width'),
    ),
  );

  return $form;
}

