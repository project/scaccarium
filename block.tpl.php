<?php
/**
 * Change block headings from <h2> to <h4>
 */
?>

<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="block block-<?php print $block->module; ?>">
  <?php if (!empty($block->subject)): ?>
    <h4><?php print $block->subject; ?></h4>
  <?php endif; ?>

  <div class="content"><?php print $block->content; ?></div>
</div>

