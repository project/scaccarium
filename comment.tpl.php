<?php
/**
 * Theme comments
 */
?>

<div class="comment<?php if (!empty($comment->new)) print ' comment-new'; ?> <?php print $status; ?> clear-block">

  <?php if (!empty($comment->new)): ?>
    <span class="new"><?php print $new; ?></span>
  <?php endif; ?>

  <h4><?php print $title; ?></h4>

  <?php if (!empty($submitted)): ?>
    <div class="submitted"><?php print $submitted; ?></div>
  <?php endif; ?>

  <div class="content">
    <?php print $content; ?>
    <?php if (!empty($signature)): ?>
      <div class="user-signature clear-block"><?php print $signature; ?></div>
    <?php endif; ?>
  </div>

  <?php if (!empty($links)): ?>
    <div class="meta"><?php print $links; ?></div>
  <?php endif; ?>

</div>

