<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>

  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>

</head>
<body class="<?php print $body_classes; ?>">

  <div id="page">
    <div id="header-outer"><div id="header"><div id="header-inner">

      <div id="title">
        <?php if (!empty($site_name)): ?>
          <h1 id="site-name"><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></h1>
        <?php endif; ?>

        <?php if (!empty($site_slogan)): ?>
          <div id="site-slogan"><?php print $site_slogan; ?></div>
        <?php endif; ?>
      </div> <!-- /title -->

      <?php if (!empty($primary_links)): ?>
        <div id="primary"><?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?></div>
      <?php endif; ?>

      <?php print $search_box; ?>

    </div></div></div> <!-- /header-inner /header /header-outer -->
    <div id="container"><div id="container-inner" class="clear-block">
      <div id="main"><div id="main-inner" class="clear-block">

        <?php if (!empty($breadcrumb) || !empty($mission) || !empty($content_top) || !empty($messages)): ?>
          <div id="content-top-region">

            <?php print $breadcrumb; ?>

            <?php if (!empty($mission)): ?>
              <div id="mission"><p><?php print $mission; ?></p></div>
            <?php endif; ?>

            <?php if (!empty($content_top)): ?>
              <div id="content-top"><?php print $content_top; ?></div>
            <?php endif; ?>

            <?php print $messages; ?>

          </div> <!-- /content-top-region -->
        <?php endif; ?>
        <div id="content"><div id="content-inner">

          <?php if (!empty($node_top)): ?>
            <div id="node-top"><?php print $node_top; ?></div>
          <?php endif; ?>

          <?php if (!empty($title)): ?>
            <h2 id="page-title"><?php print $title; ?></h2>
          <?php endif; ?>

          <?php print $help; ?>

          <?php if (!empty($tabs)): ?>
            <div class="tabs"><?php print $tabs; ?></div>
          <?php endif; ?>

          <?php print $content; ?>

          <?php print $feed_icons; ?>

          <?php if (!empty($node_bottom)): ?>
            <div id="node-bottom"><?php print $node_bottom; ?></div>
          <?php endif; ?>

        </div></div> <!-- /content-inner /content -->

        <?php if (!empty($right)): ?>
          <div id="sidebar-right"><div id="sidebar-right-inner">
            <?php print $right; ?>
          </div></div> <!-- /sidebar-right-inner /sidebar-right -->
        <?php endif; ?>

        <?php if (!empty($content_bottom)): ?>
          <div id="content-bottom"><?php print $content_bottom; ?></div>
        <?php endif; ?>

      </div></div> <!-- /main-inner /main -->

      <?php if (!empty($secondary_links) || !empty($left)): ?>
        <div id="sidebar-left"><div id="sidebar-left-inner">
          <?php if (!empty($secondary_links)): ?>
            <div id="secondary"><?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?></div>
          <?php endif; ?>
          <?php print $left; ?>
        </div></div> <!-- /sidebar-left-inner /sidebar-left -->
      <?php endif; ?>

    </div></div> <!-- /container-inner /container -->

    <?php if (!empty($footer) || !empty($footer_message)): ?>
      <div id="footer"><div id="footer-inner" class="clear-block">
        <?php if (!empty($footer)): ?>
          <div id="footer-region" class="clear-block"><?php print $footer; ?></div>
        <?php endif; ?>

        <?php if (!empty($footer_message)): ?>
          <div id="footer-message"><?php print $footer_message; ?></div>
        <?php endif; ?>
      </div></div> <!-- /footer-inner /footer -->
    <?php endif; ?>

    <!-- Extra div for Knight/Bishop image -->
    <div id="knight-bishop-outer"><div id="knight-bishop"><div id="knight-bishop-inner"></div></div></div>

  </div> <!-- /page -->

  <?php print $closure; ?>

</body>
</html>

