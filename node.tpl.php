<?php
/**
 * Theme nodes
 */
?>

<div id="node-<?php print $node->nid; ?>" class="node<?php if (!empty($sticky)) print ' sticky'; ?><?php if (empty($status)) print ' node-unpublished'; ?> clear-block">

  <?php if (empty($page)): ?>
    <h3 class="title"><a href="<?php print $node_url; ?>" title="<?php print $title; ?>"><?php print $title; ?></a></h3>
  <?php endif; ?>

  <?php if (!empty($submitted)): ?>
    <div class="submitted"><?php print $submitted; ?></div>
  <?php endif; ?>

  <div class="content"><?php print $content; ?></div>

  <?php if (!empty($links) || !empty($terms)): ?>
    <div class="meta<?php if (!empty($links)) print ' has_links'; ?><?php if (!empty($terms)) print ' has_terms'; ?> clear-block">

      <?php if (!empty($links)): ?>
        <div class="node-links"><?php print $links; ?></div>
      <?php endif; ?>

      <?php if (!empty($terms)): ?>
        <div class="terms"><?php print $terms; ?></div>
      <?php endif; ?>

    </div> <!-- /meta -->
  <?php endif; ?>

</div>

