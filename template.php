<?php
/**
 * Initialize theme settings
 */
if (is_null(theme_get_setting('scaccarium_layout'))) {
  global $theme_key;

  $defaults = array(
    'scaccarium_layout' => 'fluid',
  );

  $settings = theme_get_settings($theme_key);

  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      unset($settings['toggle_node_info_' . $type]);
    }
  }

  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge($defaults, $settings)
  );

  theme_get_setting('', TRUE);
}

/**
 * Theme links
 *
 * Add extra <span> to Primary links
 */
function scaccarium_links($links, $attributes = array('class' => 'links')) {
  global $language;
  $output = '';

  if (count($links) > 0) {
    $output = '<ul'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
          && (empty($link['language']) || $link['language']->language == $language->language)) {
        $class .= ' active';
      }
      $output .= '<li'. drupal_attributes(array('class' => $class)) .'>';

      // Add extra <span> for Primary links
      if ($attributes['class'] == 'links primary-links') {
        $output .= '<span class="bg-image">';
      }

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }

      // Close extra <span> for Primary links
      if ($attributes['class'] == 'links primary-links') {
        $output .= '</span>';
      }

      $output .= '</li>';

      $i++;
      $output .= "\n";
    }

    $output .= '</ul>';
  }

  return $output;
}

/**
 * Theme breadcrumbs
 *
 * Change divider to '::'
 */
function scaccarium_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' :: ', $breadcrumb) .'</div>';
  }
}

/**
 * Edit page variables
 *
 * Left sidebar also includes secondary links
 * Add additional class to <body> when content-top-region empty
 */
function scaccarium_preprocess_page(&$variables) {
  // Get array of body classes
  $body_classes = explode(' ', $variables['body_classes']);

  // Remove any references to sidebars (reset below)
  foreach ($body_classes as $id => $body_class) {
    if (strpos($body_class, 'sidebar') !== FALSE) {
      unset($body_classes[$id]);
    }
  }

  // Reset 'layout'
  $variables['layout'] = 'none';
  if (!empty($variables['secondary_links']) || !empty($variables['left'])) {
    $variables['layout'] = 'left';
  }
  if (!empty($variables['right'])) {
    $variables['layout'] = ($variables['layout'] == 'left') ? 'both' : 'right';
  }

  // Add sidebars to body classes
  if ($variables['layout'] == 'both') {
    $body_classes[] = 'two-sidebars';
  }
  elseif ($variables['layout'] == 'none') {
    $body_classes[] = 'no-sidebars';
  }
  else {
    $body_classes[] = 'one-sidebar sidebar-'. $variables['layout'];
  }

  // Add content top region to body classes
  if (empty($variables['breadcrumb']) && empty($variables['mission']) && empty($variables['content_top']) && empty($variables['messages'])) {
    $body_classes[] = 'content-top-region-empty';
  }

  // Add layout type to body classes
  $body_classes[] = theme_get_setting('scaccarium_layout');

  // Reset 'body_classes'
  $variables['body_classes'] = implode(' ', $body_classes);
}

/**
 * Theme tab links
 *
 * Add additional class to primary tabs when secondary tabs exist
 */
function scaccarium_menu_local_tasks() {
  $primary_output = '';
  $secondary_output = '';
  $primary_class = 'tabs primary';
  $secondary_class = 'tabs secondary';

  if ($secondary = menu_secondary_local_tasks()) {
    $primary_class .= ' has_secondary';
    $secondary_output .= "<ul class=\"". $secondary_class ."\">\n". $secondary ."</ul>\n";
  }

  if ($primary = menu_primary_local_tasks()) {
    $primary_output .= "<ul class=\"". $primary_class ."\">\n". $primary ."</ul>\n";
  }

  return $primary_output . $secondary_output;
}

/**
 * Theme form elements
 *
 * Remove label from theme search form
 */
function scaccarium_form_element($element, $value) {
  // Just output the textfield for the theme search form
  if ($element['#name'] == 'search_theme_form') {
    return $value;
  }

  // This is also used in the installer, pre-database setup.
  $t = get_t();

  $output = '<div class="form-item"';
  if (!empty($element['#id'])) {
    $output .= ' id="'. $element['#id'] .'-wrapper"';
  }
  $output .= ">\n";
  $required = !empty($element['#required']) ? '<span class="form-required" title="'. $t('This field is required.') .'">*</span>' : '';

  if (!empty($element['#title'])) {
    $title = $element['#title'];
    if (!empty($element['#id'])) {
      $output .= ' <label for="'. $element['#id'] .'">'. $t('!title: !required', array('!title' => filter_xss_admin($title), '!required' => $required)) ."</label>\n";
    }
    else {
      $output .= ' <label>'. $t('!title: !required', array('!title' => filter_xss_admin($title), '!required' => $required)) ."</label>\n";
    }
  }

  $output .= " $value\n";

  if (!empty($element['#description'])) {
    $output .= ' <div class="description">'. $element['#description'] ."</div>\n";
  }

  $output .= "</div>\n";

  return $output;
}

